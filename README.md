# Demo of non-blocking vs blocking
This application is meant to be run together with the consumer app. Producer simulates an application performing some kind of intense operation. I will log when it starts working an when it is finished sending all data. The consumer app is responsible for doing the queries to this application. 

# Build JAR
Build the application with 
`mvn clean package`

# Run application
`java -jar consumer-0.0.1-SNAPSHOT.jar`
