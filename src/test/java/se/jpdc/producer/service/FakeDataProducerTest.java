package se.jpdc.producer.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class FakeDataProducerTest {

    FakeDataProducer producer;

    @BeforeEach
    public void before() {
        producer = new FakeDataProducer();
    }

    @Test
    void testGetSuperImportantNumbers() {
        producer.getSuperImportantNumbers()
                .doOnNext(System.out::println)
                .doOnComplete(() -> System.out.println("I have sent all data"))
                .subscribe();
    }

}