package se.jpdc.producer.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import se.jpdc.producer.service.FakeDataProducer;

import java.util.List;

import static org.springframework.http.MediaType.TEXT_EVENT_STREAM_VALUE;

@RestController
@RequestMapping("data")
public class DataController {

    FakeDataProducer dataService;

    @Autowired
    public DataController (FakeDataProducer dataService) {
        this.dataService = dataService; 
    }

    @GetMapping(produces = TEXT_EVENT_STREAM_VALUE)
    public Flux<String> getImportantData () {
        return dataService.getSuperImportantNumbers();
    }

    @GetMapping("old")
    public List<String> getImportantDataOldWay () {
        return dataService.getSuperImportantNumbersOldWay();
    }
}
