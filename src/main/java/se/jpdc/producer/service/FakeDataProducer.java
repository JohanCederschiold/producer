package se.jpdc.producer.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class FakeDataProducer {

    Logger logger = LoggerFactory.getLogger(FakeDataProducer.class);

    Random random = new Random();

    private static final int VOLUME = 20;

    public Flux<String> getSuperImportantNumbers () {
        logger.info("Starting work");
        long start = Instant.now().toEpochMilli();
        return Flux.create(fluxSink -> {
            int counter = 0;
            do {
                fluxSink.next(getSuperImportantNumber());
                counter++;
            } while ( counter < VOLUME);
            logger.info(String.format("I have now sent all data. It took: %d", (Instant.now().toEpochMilli() - start)));
            fluxSink.complete();
        });
    }

    @SuppressWarnings("squid:S2629")
    public List<String> getSuperImportantNumbersOldWay () {
        logger.info("Starting work");
        long start = Instant.now().toEpochMilli();
        List<String> requestedData = new ArrayList<>();
        for (int i = 0; i < VOLUME; i++) {
            requestedData.add(getSuperImportantNumber());
        }
        logger.info(String.format("I have now sent all data. It took: %d", (Instant.now().toEpochMilli() - start)));
        return requestedData;
    }


    public String getSuperImportantNumber () {
        String now = "";
        String randomVal = Integer.toString(random.nextInt(999));
        while (!now.endsWith(randomVal)) {
            now = Long.toString(Instant.now().toEpochMilli());
        }
        return randomVal;
    }
}
